'use strict';

//***** a linked list only needs two properties, a head and a tail. ****
function Linked(){
  this.head = null;
  this.tail = null;
};

//***a node has three properties, a value, and two pointers {(one points to the next node and one points to the previous node) ***
function Node(value, next, prev) {
  this.value = value;
  this.next = next;
  this.prev = prev;
};

//***adding a new node to the head of the linked list by adding a method to the linked list prototype. ***
Linked.prototype.addToHead = function(value){
  
  //create a new node and set it to a variable. we get the value from the value we pass into the method, 'head' is this.head since we're adding this node to the head of the list, and prev will be null since there is nothing before the head.  
  let newNode = new Node(value, this.head, null);
  
  //now we check to see if there are existing nodes by seeing if 'this.head' exist, if so we set this.head.prev to our newNode variable making it the NEW head and keeping the chain intact.   
  if(this.head) this.head.prev = newNode;
  else this.tail = newNode;
  this.head = newNode;
};

//**** adding a new node to the tail of a linked list by adding a method to the prototype of the linked list constructor
Linked.prototype.addToTail = function(value) {

  //create new Node to insert at the tail of the linked list and set it to a variable. The "next parameter will be null since it will become the last node in the list."
  let newNode = new Node(value, null, this.tail);

  //check to see if the list is empty by looking to see if the tail exist. if so, set the "next value to the new node."
  if(this.tail) this.tail.next = newNode;

  //otherwise the head is the new node since it will be the only node in the list.
  else this.head = newNode;

  //set this.tail pointer to the new node.
  this.tail = newNode;
};

//**** remove node from the head of the linked list by adding a method to the linked list prototype
Linked.prototype.removeFromHead = function() {

  //check to make sure the linked list is not empty
  if(!this.head) return null;

  //set the value of the head to a variable
  let val = this.head.value;

  //re-assign the this.head.next to this.head before we remove the present head
  this.head = this.head.next;

  //if there was only one node, once removed the new head node will be null otherwise it will be the next node. We need to account for both situations by setting the prev value to null.
  if(this.head) this.head.prev = null;

  //otherwise set the tail to null since the head is already null because the list is empty.
  else this.tail = null;

  return val;
}

//**** remove node from the tail of the linked list by adding a method to the linked list prototype
Linked.prototype.removeFromTail = function() {

  //check to see if the linked list is empty
  if(!this.tail) return null;

  //assign the value of the tail node to a variable
  let val = this.tail.value;

  //re-assign the tail to the the tails previous node
  this.tail.next = null;

  //if there was only one node, once removed the new tail node will be null otherwise it will be the previous node. We need to account for both situations by setting the next value to null.
  if(this.tail) this.tail.next = null;

  //otherwise set the head to null since the tail is already null because the list is empty.
  else this.head = null;
  
  return val;
};

//**** search a linked list for a value by adding a method to prototype. 
Linked.prototype.search = function(searchValue){

  // create a variable that will hold the starting point for itefrating through the linked list.
  let currentNode = this.head;

  //use a while loop to iterate through the linked list comparing the search value to the  current node value set in the variable.
  while (currentNode) {
    if(searchValue === currentNode.value) return currentNode.value;

    //move to the next node
    currentNode = currentNode.next;
  }
  //else return null if no value is found
  return null;
};

//*** find the index of a node in a linked list by adding a method to the prototype.
Linked.prototype.indexOf = function(value) {

  //create an array to hold search values.
  let indexes = [];

  //defign a starting point and set it to a variable.
  let currentNode = this.head;

  //define a counter
  let currentIndex = 0;

  //start while loop and iterate through the linked list
  while (currentNode) {

    //search for value.
    if(currentNode.value === value) indexes.push(currentIndex);

    //move to next node
    currentNode = currentNode.next;

    //increase counter
    currentIndex++;
  }
  //expose array for access to indexes.
  return indexes;
};

//create a new linked list and apply the new methods. log out the result to the console.
let linked = new Linked();
linked.addToHead(100);
linked.addToHead(200);
linked.addToTail(250);
console.log(linked);
console.log(linked.removeFromTail());
console.log(linked.indexOf(200));
